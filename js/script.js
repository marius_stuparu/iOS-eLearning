var deviceType, lesson, errCount;

/* --------------------------------------------
 *   Detect the mobile capabilities and model
   --------------------------------------------*/
var detectMobileType = function(){
	try {
		// detect capabilities - does not return
		if(DetectWebkit()){
			$('body').addClass('webkit');
		}
		
		if(DetectOperaMobile()){
			$('body').addClass('opera');
		}
		
		// detect device type - returns on first found
		if(DetectIphone() || DetectIpod()){
			$('body').addClass('ipod iphone');
			deviceType = 'iphone';
			return;
		}
		
		if (DetectIpad()){
			$('body').addClass('ipad')
			deviceType = 'ipad';
			return;
		}
		
		if (DetectAndroidPhone()){
			$('body').addClass('androidphone')
			deviceType = 'androidphone';
			return;
		}
		
		if (DetectAndroidTablet()){
			$('body').addClass('androidtablet')
			deviceType = 'androidtablet';
			return;
		}
		
		if (DetectWindowsPhone7()){
			$('body').addClass('windowsphone7')
			deviceType = 'windowsphone7';
			return;
		}
		
		if (DetectWindowsMobile()){
			$('body').addClass('windowsphone')
			deviceType = 'windowsphone';
			return;
		}
		
		if (DetectBlackBerryTouch()){
			$('body').addClass('blackberrytouch')
			deviceType = 'blackberrytouch';
			return;
		}
		
		if (DetectBlackBerryTablet()){
			$('body').addClass('blackberrytablet')
			deviceType = 'blackberrytablet';
			return;
		}
		
		if (DetectBlackBerry()){
			$('body').addClass('blackberry')
			deviceType = 'blackberry';
			return;
		}
		
		if (DetectPalmOS()){
			$('body').addClass('palm')
			deviceType = 'palm';
			return;
		}
		
		if (DetectPalmWebOS()){
			$('body').addClass('palmwebos')
			deviceType = 'palmwebos';
			return;
		}
		
		if (DetectWebOSTablet()){
			$('body').addClass('palmtablet')
			deviceType = 'palmtablet';
			return;
		}
		
		if (DetectMaemoTablet()){
			$('body').addClass('maemotablet')
			deviceType = 'maemotablet';
			return;
		}
		
		if (DetectKindle()){
			$('body').addClass('kindle')
			deviceType = 'kindle';
			return;
		}
		
		$('body').addClass('pc')
	} catch(err){
		var errorBody = err.message.split(':')
		if (errorBody[0] == "Can't find variable"){
			$('#container')
				.removeClass()
				.addClass('error')
				.html('<div class="error_message"><h3>Fatal error!</h3><p>A very important file failed to load: <span class="blame">"js/libs/mdetect.js"</span></p><p>Please contact support!</p></div>')
		}
	}
}

var runLesson = function(){
	if(typeof AeL_framework == 'function'){ // check if framework has loaded
		lesson = new AeL_framework(); // lesson loads, no need to do anything else here
	} else { // if it hasn't, retry after 5 seconds
		errCount++
		
		if(errCount < 4) {
			setTimeout("runLesson()",1000); // wait for the framework to load
		} else {
			$('#container')
				.removeClass()
				.addClass('error')
				.html('<h1>Fatal error!</h1><p>A very important file failed to load: <span class="blame">"js/libs/ael_framework.js"</span></p><p>Please contact support!')
		}
	}
}

// initialise lesson
$(function(){
	detectMobileType(); // uses MobileESP, so make sure mdetect.js is loaded before	http://localhost/newui/img/background/ipad-landscape-error.jpg
	
	try {
		if(DetectIos()){
			$(document).bind('touchmove', function(e){
				e.preventDefault() // prevent top/bottom bounce on iOS
			})
		}
	} catch(err){
		var errorBody = err.message.split(':')
		if (errorBody[0] == "Can't find variable"){
			$('#container')
				.removeClass()
				.addClass('error')
				.html('<div class="error_message"><h3>Fatal error!</h3><p>A very important file failed to load: <span class="blame">"js/libs/mdetect.js"</blame></p><p>Please contact support!</p></div>');
			return;
		}
	}

	runLesson();
})
/*
 * Developed by Marius Stuparu for Siveco Romania SA
 * Copyright (c) 2012 Siveco Romania SA http://www.siveco.ro
 *
 * Version 0.8 beta
 *
 * Usage:
 *
 * If you're not using the complete framework:
 * 		- make sure you load jQuery (minimum version 1.7)
 * 		- load the iScroll plugin first (needed by the sidebar pagination) <script src="js/libs/iscroll.js"></script>
 * 		- load the script in the main HTML file: <script src="js/ael_framework.js"></script>
 * 		- call the framework in your main script: var lesson = new AeL_framework();
 * 		- you can set up a call-back function to read device orientation changes: var lesson = new AeL_framework(function(orientation){...});
 * 		  (the returned string will be "portrait" or "landscape")
 *
 * There are some public variables that you can read from outside:
 * 		lesson.currentPage - self explanatory
 * 		lesson.totalPages - set from the number of pages in settings.js
 * 		lesson.errorThrown - set to true if there's an error while loading lesson files
 * 		lesson.deviceOrientation - "portrait" or "landscape" (this is the var returned to the call-back)
 * 		lesson.currentInstructions - the current "to do"/instruction for the lesson (shown by the popup attached to the footer button)
 *
 * Thery're also a few public methods (more detailed usage inside each method's definition)
 * 		lesson.loadPage(pageNumber) - not usually needed, but you can jump to any page with this
 * 		lesson.popupDialogCreate(element, title, content, type, autoshow, pop_width, pop_height)
 * 			- create a popup dialog centered on "element", with "title" and "content", of type "text" or "extras", not/shown automatically, and of pop_width x pop_height
 * 			  this destroys the previous popup, so make sure you save its contents
 * 		lesson.popupDialogShow() - show the current popup (if hidden) - only one at the time, to not confuse the code
 * 		lesson.popupDialogHide() - hide the current popup for reuse
 *  	lesson.popupDialogDestroy() - remove the "#popup" div from DOM
 * 		lesson.setNotification(element) - set a "!" notification on any button identified by "element"
 * 		lesson.clearNotification(element) - remove the "!" notification from "element"
 * 		lesson.lightbox(resource_type, resource_url, video_width, video_height)
 * 			- display an image or video lightbox-like style; resource_type must be "photo" or "video"
 * 			  image dimensions are autodetected, but video dimensions must be specified (at least one)
 */

;(function($) {
    AeL_framework = function(callBackFn) {

        var defaults = {
            lessonLocation: 'lesson/', // be careful when placing lesson files in another location, could lead to unforseen errors
            MAX_WORD_COUNT_PORTRAIT: 450, // override these if text does't flow OK in theory
            MAX_WORD_COUNT_LANDSCAPE: 700
        }

        var plugin = this;

        // define private data
        var theoryContent = {
	        	landscape: [],
	        	portrait: [],
	        	landscapeMultipage: false,
	        	portraitMultipage: false,
	        	landscapeContainer: $('#theory_landscape'),
	        	portraitContainer: $('#theory_portrait'),
	        	width: 0,
	        	height: 0
	        },
        	scroller = undefined,
        	resizeEvent = false;

        // define public data
        plugin.instructions = $('aside.instructions');
        plugin.theory_container = $('aside.theory');
        plugin.currentPage = 1; // initial page is always 1
        plugin.totalPages = 0;
        plugin.errorThrown = false; // check this for file load issues
        plugin.deviceOrientation = undefined; // gets updated by updateOrientation(), can be 'portrait' or 'landscape'
        plugin.callBackFn = callBackFn; // used this to retrieve orientation from device
        plugin.appVersion = 'SIVECO AeL for mobile tablets, 0.8b';

        plugin.settings = {}

        plugin.init = function() {
            plugin.settings = $.extend({}, defaults);

            updateOrientation();

            loadSettings();

            if(!plugin.errorThrown){
            	plugin.loadPage(plugin.currentPage) // load the first page
            }
        }

        /* --------------------------------------------
		 *         Load and populate content
		 *          (called by plugin.init)
		   --------------------------------------------*/
        var loadSettings = function(){
			$.ajax({
				url: plugin.settings.lessonLocation + 'js/settings.js',
				dataType: 'script',
				async: false,
				success: function(){
					pageInit()
				},
				error: function(x, e){
					errorThrown = true;
					if(x.status==404){
						alert('An important file (settings.js) is missing.\nPlease contact support.');
					}else if(x.status==500){
						alert('Internal Server Error.');
					}else if(e=='timeout'){
						alert('Request Time out.');
					}else {
						alert('Unknow Error.\n'+x.responseText);
					}
					return false;
				}
			})
		}

		/* --------------------------------------------
		 *            (called by loadSettings)
		   --------------------------------------------*/
		var pageInit = function(){
			var breadcrumb = $('#breadcrumbs'),
				footer = $('footer'),
				nav_bullets = [];

			plugin.totalPages = page.length - 1;

			document.title = settings.courseTitle + ' : ' + settings.lessonTitle;

			// add the breadcrumb elements in <header />
			breadcrumb
				.find('ul')
				.append('<li role="listitem banner" class="course_title">' + settings.courseTitle + '</li><li role="presentation" class="separator"></li><li role="listitem banner" class="lesson_title">' + settings.lessonTitle + '</li>')
				.end()

			$('#ael_logo').addClass(settings.generalDirection)

			if(plugin.totalPages == 1){
				footer
					.find('.nav_next')
					.removeClass('enabled')
					.addClass('disabled')
					.end()
			}

			// insert navigation in the footer
			footer.html('<a href="#" class="nav_prev disabled" role="link" style="direction:' + settings.generalDirection + '"><span>' + settings.arrowPrevText + '</span></a><p></p><a href="#" class="nav_next enabled" role="link" style="direction:' + settings.generalDirection + '"><span>' + settings.arrowNextText + '</span></a>')

			// move things around according to writing direction
			if(settings.generalDirection == 'rtl'){
				// right-to-left languages
				breadcrumb
					.css('float', 'right')
					.find('li')
					.css({
						'float': 'right',
						'direction': settings.generalDirection
						})
					.end()

				$('#help').css({
					'float': 'left',
					'direction': settings.generalDirection
					})

				footer
					.find('.nav_prev')
					.css('float', 'right')
					.end()
					.find('.nav_next')
					.css('float', 'left')
					.end()

				// create bullets for each page in the lesson
				for(page_nr = plugin.totalPages; page_nr > 1; page_nr--){
					nav_bullets.push('<span role="listitem" class="nav_bullet bullet_page_' + page_nr + '"></span>')
				}

				nav_bullets.push('<span role="listitem" class="nav_bullet bullet_page_1 current"></span>')

				footer
					.find('p')
					.html(nav_bullets.join(''))
			} else {
				// left-to-right languages
				breadcrumb
					.css('float', 'left')
					.find('li')
					.css({
						'float': 'left',
						'direction': settings.generalDirection
						})
					.end()

				$('#help').css({
					'float': 'right',
					'direction': settings.generalDirection
					})

				footer
					.find('.nav_prev')
					.css('float', 'left')
					.end()
					.find('.nav_next')
					.css('float', 'right')
					.end()

				// create bullets for each page in the lesson
				nav_bullets.push('<span role="listitem" class="nav_bullet bullet_page_1 current"></span>')

				for(page_nr=2; page_nr < plugin.totalPages + 1; page_nr++){
					nav_bullets.push('<span role="listitem" class="nav_bullet bullet_page_' + page_nr + '"></span>')
				}

				footer
					.find('p')
					.html(nav_bullets.join(''))
			}

			setupEventListeners()
		}

		var setupEventListeners = function(){
			// cancel scroll / bounce for page
			$(document).on('touchmove', function(e){
				e.preventDefault();
			})

			$('header')
				.on('touchstart click', '#ael_logo', returnToLibrary)
				.on('touchstart click', '#help a', displayHelp);

			$('footer')
				.on('touchstart click', '.nav_prev.enabled', previousPage)
				.on('touchstart click', '.nav_next.enabled', nextPage);
		}

		var returnToLibrary = function(e){
			e.preventDefault(); // prevent loading href in browser window
			// TO DO
			alert('This will go back to the Library');
			return;
		}

		var displayHelp = function(e){
			e.preventDefault(); // prevent loading href in browser window
			// TO DO
			alert('This will display the Help');
			return;
		}

		var previousPage = function(e){
			var footer = $('footer'),
				nav_prev = footer.find('.nav_prev'),
				nav_next = footer.find('.nav_next');

			e.preventDefault(); // prevent loading href in browser window

			if(plugin.currentPage > 1){ // just to make sure, shouldn't trigger anyway
				// change the bullet
				footer
					.find('.bullet_page_' + plugin.currentPage)
					.removeClass('current')
					.end()
					.find('.bullet_page_' + (plugin.currentPage - 1))
					.addClass('current')
					.end()

				// disable prev button if we load first page
				if(plugin.currentPage == 2){
					nav_prev
						.removeClass('enabled')
						.addClass('disabled')
				}

				// enable next if it was disabled and there are more than one pages
				if(nav_next.hasClass('disabled') && plugin.totalPages > 1){
					nav_next
						.addClass('enabled')
						.removeClass('disabled')
				}

				$('aside.instructions').html('');

				// decrease the current page index then load
				plugin.loadPage(--plugin.currentPage)
				return;
			} else {
				return;
			}
		}

		var nextPage = function(e){
			var footer = $('footer'),
				nav_prev = footer.find('.nav_prev'),
				nav_next = footer.find('.nav_next');

			e.preventDefault(); // prevent loading href in browser window

			if(plugin.currentPage < plugin.totalPages){ // just to make sure, shouldn't trigger anyway
				// change the bullet
				footer
					.find('.bullet_page_' + plugin.currentPage)
					.removeClass('current')
					.end()
					.find('.bullet_page_' + (plugin.currentPage + 1))
					.addClass('current')
					.end()

				// disable next button if we load last page
				if(plugin.currentPage == (plugin.totalPages - 1)){
					nav_next
						.removeClass('enabled')
						.addClass('disabled')
				}

				// enable prev if it was disabled and there are more than one pages
				if(nav_prev.hasClass('disabled') && plugin.totalPages > 1){
					nav_prev
						.removeClass('disabled')
						.addClass('enabled')
				}

				$('aside.instructions').html('');

				// increase the current page index then load
				plugin.loadPage(++plugin.currentPage)
				return;
			} else {
				return;
			}
		}

		/* --------------------------------------------
		 *        Push content into sidebar
		 *       (called by plugin.loadPage)
		   --------------------------------------------*/
		var placeTheory = function(){
			theoryContent.portrait = page[plugin.currentPage].theory;
			theoryContent.landscape = page[plugin.currentPage].theory;

			// push the theory into containers
			$('#theory_portrait')
				.addClass(settings.generalDirection)
				.html(theoryContent.portrait)
			$('#theory_landscape')
				.addClass(settings.generalDirection)
				.html(theoryContent.landscape)

			// if content is too large, split it into pages for iScroll
			if($('#theory_portrait').html().length > plugin.settings.MAX_WORD_COUNT_PORTRAIT){
				theoryContent.portraitMultipage = true;
				theoryContent.portrait = splitTextIntoUl('#theory_portrait', plugin.settings.MAX_WORD_COUNT_PORTRAIT)
			}

			if($('#theory_landscape').html().length > plugin.settings.MAX_WORD_COUNT_LANDSCAPE){
				theoryContent.landscapeMultipage = true;
				theoryContent.portrait = splitTextIntoUl('#theory_landscape', plugin.settings.MAX_WORD_COUNT_LANDSCAPE)
			}

			toggleTheory();
		}

		/* --------------------------------------------
		 *         Split text into multiple LIs
		 *   based on the max character count per page
		 *          (called by placeTheory)
		   --------------------------------------------*/
		var splitTextIntoUl = function(element, characterCount){
			var paginateElement = $(element),
				children = paginateElement.children().get(),
				numberOfChildren = children.length,
				currentLength = 0, checkSum = 0, currentLi = '',
				pageNumber = 1, ulContent = [];

			ulContent.push('<ul class="pages">')

			paginateElement.children()
				.each(function(childNumber){
					checkSum = currentLength + children[childNumber].outerHTML.length;

					if(checkSum <= characterCount){
						// if we don't break the maximum length, add the next child
						// to the stack and update the current character count
						currentLength += children[childNumber].outerHTML.length;
						currentLi += children[childNumber].outerHTML
					} else {
						// if we reach the target count, push the collected children
						ulContent.push('<li class="page_' + pageNumber + '">' + currentLi + '</li>')
						// and do some cleanup
						pageNumber++;
						currentLength = 0;
						currentLi = ''
					}
				})

			if (currentLi != ''){
				ulContent.push('<li class="page_' + pageNumber + '">' + currentLi + '</li>')
			}

			// final touch to the list
			ulContent.push('</ul>')
			// replace original text with the list
			paginateElement.html(ulContent.join(''))
			// also return the result for any further use
			return ulContent.join('');
		}

		/* --------------------------------------------
		 *     Show and hide the panels in sidebar
		 * (called by placeTheory, orientationChanged)
		   --------------------------------------------*/
		var toggleTheory = function(){
			if(plugin.deviceOrientation == 'portrait'){
				theoryContent.landscapeContainer.hide()
				theoryContent.portraitContainer.fadeIn(500)

				theoryContent.width = theoryContent.portraitContainer.width()
				theoryContent.height = theoryContent.portraitContainer.height()

				if(theoryContent.portraitMultipage){
					setupScroller(theoryContent.portraitContainer)
				}
			} else {
				theoryContent.portraitContainer.hide()
				theoryContent.landscapeContainer.fadeIn(500)

				theoryContent.width = theoryContent.landscapeContainer.width()
				theoryContent.height = theoryContent.landscapeContainer.height()

				if(theoryContent.landscapeMultipage){
					setupScroller(theoryContent.landscapeContainer)
				}
			}
		}

		/* --------------------------------------------
		 *            Create sidebar pages
		 *          (called by setupSidebar)
		   --------------------------------------------*/
		var setupScroller = function(container){
			var numPages = container.find('li').length,
				navigation = '<p class="theory_nav"><span class="container roundedCorners"><span class="navBullet navBulletActive"></span>',
				containerID = container.attr('id');

			// create navigation buttons
			for(var iter = 2; iter < numPages + 1; iter++){
				navigation += '<span class="navBullet navBulletInactive"></span>'
			}

			navigation += '</span></p>';
			container
				.append(navigation)
				.find('ul')
				.css({
					'width': (numPages * (theoryContent.width + 10)) + 'px',
					'height': theoryContent.height + 'px'
				})
				.find('li')
				.css({
					'width': theoryContent.width + 'px',
					'height': theoryContent.height + 'px'
				})
				.end()

			if(typeof iScroll == 'function'){
				// only one at the time, otherwise it _will_ affect performance
				scroller = new iScroll(containerID, {
					snap:true,
					bounce:false,
					momentum:false,
					hScrollbar: false,
					vScroll: false,
					onScrollEnd: function () {
						// switch the nav bullets
						container
							.find('.navBullet')
							.removeClass('navBulletActive')
							.addClass('navBulletInactive')
							.end()
							.find('.theory_nav .navBullet:nth-child(' + (this.currPageX + 1) + ')')
							.removeClass('navBulletInactive')
							.addClass('navBulletActive')
							.end();
					}
				});
			} else {
				$('#container')
					.removeClass()
					.addClass('error')
					.html('<div class="error_message"><h3>Fatal error!</h3><p>A very important file failed to load: <span class="blame">"js/libs/iscroll.js"</span></p><p>Please contact support!</p></div>');
				return true;
			}
		}

		/* ----------------------------------------------------
		 *          findPos() by quirksmode.org
		 * Finds the absolute position of an element on a page
		   ---------------------------------------------------*/
		var findPos = function(obj) {
			var curleft = curtop = 0;
			if (obj.offsetParent) {
				do {
					curleft += obj.offsetLeft;
					curtop += obj.offsetTop;
				} while (obj = obj.offsetParent);
			}
			return [curleft,curtop];
		}

		/* --------------------------------------------
		 *      getPageScroll() by quirksmode.org
		 *     Finds the scroll position of a page
		   --------------------------------------------*/
		var getPageScroll = function() {
			var xScroll, yScroll;
			if (self.pageYOffset) {
				yScroll = self.pageYOffset;
				xScroll = self.pageXOffset;
			} else if (document.documentElement && document.documentElement.scrollTop) {
				yScroll = document.documentElement.scrollTop;
				xScroll = document.documentElement.scrollLeft;
			} else if (document.body) {// all other Explorers
				yScroll = document.body.scrollTop;
				xScroll = document.body.scrollLeft;
			}
			return [xScroll,yScroll]
		}

		/* --------------------------------------------
		 * findPosRelativeToViewport() by quirksmode.org
		 * Finds the position of an element relative to the viewport
		   --------------------------------------------*/
		var findPosRelativeToViewport = function(obj) {
			var objPos = findPos(obj)
			var scroll = getPageScroll()
			return {
				left: objPos[0]-scroll[0],
				top: objPos[1]-scroll[1]
			}
		}

		/* --------------------------------------------
		 *      Set up the li in the extras popup
		 *     (called by plugin.popupDialogCreate)
		   --------------------------------------------*/
		var setupExtrasPopup = function(){
			$('#popup li').each(function(index){
				var thisItem = $(this),
					thisLink = thisItem.attr('data-uri'),
					thisType = thisItem.attr('class'),
					thisWidth = thisItem.attr('data-width'),
					thisHeight = thisItem.attr('data-height');

				thisItem.wrapInner('<span />');
				thisItem.on('click', function(){
					plugin.lightbox(thisType, thisLink, thisWidth, thisHeight)
				})
			})
		}

/* --------------------------------------------
 *         Detect device orientation
 *      and trigger changes based on it
 *  (called by plugin.init and window event)
   --------------------------------------------*/
function updateOrientation() {
	// Detect whether device supports orientationchange event, otherwise fall back to the resize event
	// Genius solution from http://stackoverflow.com/a/2307936
	var supportsOrientationChange = "onorientationchange" in window,
	    orientationEvent = supportsOrientationChange ? "orientationchange" : "resize",
	    newAngle, newOrientation;

	if(supportsOrientationChange){
		newAngle = window.orientation;
		switch(newAngle){
			case 0:
			case 180: newOrientation = 'portrait'; break;
			case 90:
			case -90: newOrientation = 'landscape'; break;
		}
	} else {
		if(document.width < document.height){
			newOrientation = 'portrait'
		} else {
			newOrientation = 'landscape'
		}
	}

	// Do the processing here

	/*
	 * Beautiful debouncing for resize event firing too much on the PC
	 * by Pim Jager http://stackoverflow.com/a/668185/930987
	 */
	resizeEvent = false;

	window.addEventListener(orientationEvent, function() {
		if(!resizeEvent) {
			clearTimeout(resizeEvent);
			resizeEvent = setTimeout(updateOrientation, 500) // half a second should be enough for a modern PC
		}
	})
}

		/* --------------------------------------------
		 *       Update everything that changes
		 *          with device orientation
		   --------------------------------------------*/
		var orientationChanged = function(){
			toggleTheory();

			// you can provide a call-back function to get notified if the orientation changes
			if(typeof plugin.callBackFn == 'function'){
		    	plugin.callBackFn (plugin.deviceOrientation);
		    }
		}

		/* --------------------------------------------
		 *               PUBLIC METHODS
		   --------------------------------------------*/

		/* --------------------------------------------
		 *             Load page by number
		   --------------------------------------------*/
		plugin.loadPage = function(pageNumber){
			// validate input
			if(typeof pageNumber == 'string'){
				pageNumber = parseInt(pageNumber)
			}

			if(typeof pageNumber != 'number'){
				console.error('Type error: the page number provided for "loadPage" method is not a number (' + pageNumber + ' is ' + (typeof pageNumber) + ')');
				return false;
			}

			if((pageNumber < 1) || (pageNumber > plugin.totalPages)){
				console.error('Out of range: the page number provided for "loadPage" method is out of range. Available range is: 1-' + plugin.totalPages + ', you called  ' + pageNumber + ')');
				return false;
			}

			var urlToPage = plugin.settings.lessonLocation + 'page_' + pageNumber + '.html'

			$.ajax({
				url: urlToPage,
				dataType: 'html',
				async: false,
				success: function(){
					if(plugin.currentPage != pageNumber) {
						plugin.currentPage = pageNumber;
					}

					placeTheory();

					$('#content')
						.find('article.stage')
						.html('<iframe src="' + urlToPage + '" id="page_' + plugin.currentPage + '"></iframe>')
						.end()
				},
				error: function(x, e){
					errorThrown = true;
					if(x.status==404){
						alert('An important file (' + plugin.settings.lessonLocation + 'page_' + pageNumber + '.html) is missing.\nPlease contact support.');
					}else if(x.status==500){
						alert('Internel Server Error.');
					}else if(e=='timeout'){
						alert('Request Time out.');
					}else {
						alert('Unknow Error.\n'+x.responseText);
					}
					return false;
				}
			})
		}

		/* --------------------------------------------
		 *        Create pop-up dialog on button
		   --------------------------------------------*/
		plugin.popupDialogCreate = function(element, title, content, type, autoshow, pop_width, pop_height){
			/*
			 * element - can be any identifyer, like #id, .class, tag or more complex '#id .class .secondclass > child'
			 * title - must be string
			 * content - must be string containing any HTML tags; for extras (see below), use only <h1>...<h5>, <hr />, <ul> and <li> with class 'photo' or 'video' and rel attribute as path to resource
			 * 		example of extras content:
			 * 		<h1>Photos</h1>
			 * 			<ul>
			 * 				<li class="photo" data-uri="lesson/img/extras1.jpg">Description</li>
			 * 				<li class="photo" data-uri="lesson/img/extras2.jpg">Description</li>
			 * 				<li class="photo" data-uri="lesson/img/extras3.jpg">Description</li>
			 * 			</ul>
			 * 		<hr />
			 * 		<h1>Video</h1>
			 * 			<ul>
			 * 				<li class="video" data-uri="lesson/video/extras1.mpg" data-width="400" data-height="300">Description</li>
			 * 				<li class="video" data-uri="lesson/video/extras2.mpg" data-width="400" data-height="300">Description</li>
			 * 				<li class="video" data-uri="lesson/video/extras3.mpg" data-width="400" data-height="300">Description</li>
			 * 			</ul>
			 * type - optional; must be string; if ommited, assumed 'text', or use 'extras'
			 * autoshow - optional; boolean; if true, dialog is created then shown; if false, it's only created as structure
			 * pop_width, pop_height - optional; specify dimensions other than defaults
			 */

			// define selectors
			var parent_element = document.querySelector(element),
				parent_element_jq = $(element);

			// define position and dimension variables
			var parent_position  = {left: undefined, right: undefined}, parent_dimensions = {width: undefined, height: undefined}, screen_size = {width: undefined, height: undefined}, center_on = {left: undefined, right: undefined},
				popup_definition = {
					left: 0,
					right: 0,
					width: 400,
					height: 475,
					type: 'text'
				};

			// define popup direction and arrow type
			var pop_up = true, // this is the default option
				pop_down = false, pop_left = false, pop_right = false,
				pop_upleft = false, pop_upright = false, pop_downleft = false, pop_downright = false,
				arrow_type;

			// validate input
			if(typeof element != 'string'){
				console.error('Type error: the element name provided for "popupDialog" method is not a string (' + element + ' is ' + (typeof element) + ')');
				return false;
			}

			if(typeof title != 'string'){
				console.error('Type error: the title provided for "popupDialog" method is not a string (' + title + ' is ' + (typeof title) + ')');
				return false;
			}

			if(typeof content != 'string'){
				console.error('Type error: the content provided for "popupDialog" method is not a string (' + content + ' is ' + (typeof content) + ')');
				return false;
			}

			if(typeof type != 'string'){
				if(type != undefined){
					console.warn('Warning: the type provided for "popupDialog" method is not a string (' + type + ' is ' + (typeof type) + '). Using default value.');
				}
			} else if (type == 'extras'){
				popup_definition.type = type;
			}

			if(typeof autoshow != 'boolean'){
				autoshow = false;
			}

			if((typeof pop_width) == 'string') { pop_width = parseInt(pop_width) } // should be number, but we can parse a string too
			if((typeof pop_width) == 'number') {
				popup_definition.width = pop_width
			} else {
				if(pop_width != undefined){
					console.warn('Warning: the pop_width option for "popupDialog" method is not a number (' + pop_width + ' is ' + (typeof pop_width) + '). Value has been disregarded, using default: ' + popup_definition.width);
				}
			}

			if((typeof pop_height) == 'string') { pop_height = parseInt(pop_height) } // should be number, but we can parse a string too
			if((typeof pop_height) == 'number') {
				popup_definition.height = pop_height
			} else {
				if(pop_height != undefined){
					console.warn('Warning: the pop_height option for "popupDialog" method is not a number (' + pop_height + ' is ' + (typeof pop_height) + '). Value has been disregarded, using default: ' + popup_definition.height);
				}
			}

			//	find out where to put it
			parent_position = findPosRelativeToViewport(parent_element);

			var margin_left = parseInt(parent_element_jq.css('margin-left'))
			var margin_top = parseInt(parent_element_jq.css('margin-top'))

			parent_dimensions.width = parent_element_jq.outerWidth();
			parent_dimensions.height = parent_element_jq.outerHeight();
			center_on.left = parent_position.left + (parent_dimensions.width / 2)
			center_on.top = parent_position.top + (parent_dimensions.height / 2)

			screen_size.width  = window.innerWidth;
			screen_size.height = window.innerHeight;

			// check popup position
			if( (center_on.left < (popup_definition.width / 2)) ){ pop_right = true }
			if( (screen_size.width - center_on.left) < (popup_definition.width / 2) ){ pop_left = true }

			if( (center_on.top < (popup_definition.height / 2)) ){ pop_down = true; pop_up = false }

			// check if popup is diagonal
			if(pop_left){
				if(pop_up){
					pop_left = false; pop_upleft = true;
				} else {
					pop_left = false; pop_downleft = true;
				}
			}

			if(pop_right){
				if(pop_up){
					pop_right = false; pop_upright = true;
				} else {
					pop_right = false; pop_downright = true;
				}
			}

			// if there's another popup left open, destroy it
			if(popupOn){
				plugin.popupDialogDestroy()
			}

			// create the popup frame
			$('#container').append('<div id="popup" class="' + popup_definition.type + '" style="display:none"><div class="popup_shadow"><div class="popup_header">' + title + '</div><div class="popup_container"><div class="popup_content">' + content + '</div></div><div class="popup_footer"></div></div></div>')

			if(popup_definition.type == 'extras'){
				setupExtrasPopup()
			}

			// compute position and determine arrow type
			if(pop_up){
				popup_definition.top = center_on.top - popup_definition.height - (parent_dimensions.height / 2);

				if (pop_upright){
					arrow_type = 'arrowDownRight';
					popup_definition.left = parent_position.left;
				} else if (pop_upleft){
					arrow_type = 'arrowDownLeft';
					popup_definition.left = parent_position.left + parent_dimensions.width - popup_definition.width;
				} else {
					arrow_type = 'arrowDown';
					popup_definition.left = center_on.left - (popup_definition.width / 2);
				}

				$('#popup').append('<div class="popupArrow ' + arrow_type + '"></div>')
			} else {
				popup_definition.top = parent_position.top + parent_dimensions.height;
				$('#popup')
					.find('.popup_shadow')
					.css({
						'top': '20px',
						'bottom': 0
					})
					.end()

				if (pop_downright){
					arrow_type = 'arrowUpRight';
					popup_definition.left = parent_position.left;
				} else if (pop_downleft){
					arrow_type = 'arrowUpLeft';
					popup_definition.left = parent_position.left + parent_dimensions.width - popup_definition.width;
				}
				else {
					arrow_type = 'arrowUp';
					popup_definition.left = center_on.left - (popup_definition.width / 2);
				}

				$('#popup').prepend('<div class="popupArrow ' + arrow_type + '"></div>')
			}

			// actually set the position and dimensions
			$('#popup')
				.css({
					'left': popup_definition.left + 'px',
					'top': popup_definition.top + 'px',
					'width': popup_definition.width + 'px',
					'height': popup_definition.height + 'px'
				})
				.find('.popup_container')
				.height(popup_definition.height - 100) // header = 40, footer = 40, arrow = 20
				.end()

			switch(arrow_type){
				case 'arrowDownRight':
				case 'arrowUpRight': $('#popup .' + arrow_type).css('left', '5px'); break;
				case 'arrowDownLeft':
				case 'arrowUpLeft': $('#popup .' + arrow_type).css('right', '5px'); break;
				case 'arrowDown':
				case 'arrowUp': $('#popup .' + arrow_type).css('left', ((popup_definition.width / 2) - 16) + 'px'); break;
			}

			if(autoshow){
				plugin.popupDialogShow();
			}
		}

		plugin.popupDialogDestroy = function(){
			$("#popup").remove()
		}

		plugin.popupDialogShow = function(){
			popupOn = true;
			$('#popup').fadeIn(750)
		}

		plugin.popupDialogHide = function(){
			popupOn = false;
			$('#popup').fadeOut(750)
		}

		// Engage!
        plugin.init();
    } // end plugin

})(jQuery);